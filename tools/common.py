import sys
import os
import json
from xml.dom import Node, minidom

if sys.version_info[0] != 3:
	raise Exception("Python 3 is required!")

parameterCount = len(sys.argv)
if parameterCount != 3:
	print("Usage ./common.py <zone> <tag>")
	exit()

zone = sys.argv[1]
tag = sys.argv[2]

print("tag =", tag)

#dom = minidom.parse("flashlists/example.flash") # or xml.dom.minidom.parseString(xml_string)
#pretty_xml_as_string = dom.toprettyxml()

def xdaqToElasticType(type):
	if type == "string":
		return "keyword"

	elif type == "bool":
		return "boolean"

	elif type == "int":
		return "integer";

	elif type == "int 32":
		return "integer"

	elif type == "unsigned int":
		return "long"

	elif type == "unsigned int 32":
		return "long"

	elif type == "unsigned int 64":
		return "long"

	elif type == "unsigned long":
		return "long"

	elif type == "unsigned short":
		return "integer"

	elif type == "time":
		return "date"

	elif type == "float":
		return "float"

	elif type == "double":
		return "double"

	elif type == "vector unsigned int 32":
		return "long"

	elif type == "vector unsigned int":
		return "long"

	elif type == "vector int 32":
		return "integer"

	elif type == "vector int":
		return "integer"

	elif type == "vector unsigned int 64":
		return "long"

	elif type == "vector float":
		return "float"

	elif type == "vector double":
		return "double"

	elif type == "vector bool":
		return "bool"

	else:
		raise Exception("Failed to convert xdata type '{0}' to elasticsearch type".format(type))
#
# convert original XML flashlist in json format (in elasticsearch _meta )
#
def generateJSONDefinition(items):
	definition = {}
	for item in items:
		if (item.nodeType == Node.ELEMENT_NODE) and (item.nodeName == "xmas:item"):
			print("item.nodeName = {0}".format(item.nodeName))
			definition[item.attributes["name"].nodeValue] = {}
			property = definition[item.attributes["name"].nodeValue]
			for attribName in item.attributes.keys():
				attribValue = item.getAttribute(attribName)
				property[attribName] = attribValue
				print("{0}: {1}".format(attribName, attribValue))
				attributeNode = item.getAttributeNode(attribName)
				if attributeNode.prefix != None:
					property["xmlns:" + attributeNode.prefix] = attributeNode.namespaceURI
				
			if len(item.childNodes) > 0:
				property["definition"] = generateJSONDefinition(item.childNodes)
	return definition

"""
          "meta_unique_key" : {
            "type" : "keyword",
            "store" : true,
            "index" : true
          },
          "meta_hash_key" : {
            "type" : "keyword",
            "store" : true,
            "index" : true
          },
          "flash_key" : {
            "type" : "keyword",
            "store" : true,
            "index" : true
          },
          "creationtime_" : {
            "type" : "date",
            "store" : false,
            "index" : true,
            "format" : "epoch_millis"
          },
          "expirationtime_" : {
            "type" : "date",
            "store" : false,
            "index" : true,
            "format" : "epoch_millis"
          },
          "withdrawtime_" : {
            "type" : "date",
            "store" : false,
            "index" : true,
            "format" : "epoch_millis"
          }
"""
# convert flashlist int Elasticsearch mapping properties (in elasticsearch mappings)
def generateJSONProperties(items):
	properties = {}
	for item in items:
		if (item.nodeType == Node.ELEMENT_NODE) and (item.nodeName == "xmas:item"):
			print("item.nodeName = {0}".format(item.nodeName))
			if item.attributes["type"].nodeValue == "table":
				#properties[item.attributes["name"].nodeValue] = { "properties" : generateJSONProperties(item.childNodes) }
				properties[item.attributes["name"].nodeValue] = {}
				properties[item.attributes["name"].nodeValue]["properties"] = generateJSONProperties(item.childNodes)
			else:
				properties[item.attributes["name"].nodeValue] = {}
				property = properties[item.attributes["name"].nodeValue]
				#!!!! set default values, it should be overriden by optional attributes
				property["type"] = xdaqToElasticType(item.attributes["type"].nodeValue)
				property["name"] = item.attributes["name"].nodeValue
				property["store"] = False
				property["index"] = False
				property["doc_values"] = False
				for attribName in item.attributes.keys():
					attributeNode = item.getAttributeNode(attribName)
					if attributeNode.localName == "store" or attributeNode.localName == "index" or  attributeNode.localName == "doc_values":
						property[attributeNode.localName] = bool(item.getAttribute(attribName))

				#!!!!!!! check in C++ code, this is derived from the haskkey provided in collector settings
				property["copy_to"] = [ "meta_hash_key" ] 
	return properties

def generateJSONFlashlist(zone, tag, domFlashlist, domSettings):
	root = domFlashlist.documentElement;
	
	id = root.getAttribute("id")
	version = root.getAttribute("version")
	key = root.getAttribute("key")
	
	#Extracting flashlistname
	qname = id
	fname = qname.split(":")[2]

	prefix = zone + "-" + fname.lower() + "-" + tag

	#<xmas:collector hashkey="context,lid" elastic:timetolive="P1Y" elastic:flashinterval="PT1M" elastic:timeinterval="P1MT17M" elastic:indexstoretype="niofs" elastic:numberofreplicas="0"/>
	settings = {}
	settings["numberofreplicas"] = "1"
	settings["indexstoretype"] = "niofs"
	settings["timeinterval"] = None
	settings["flashinterval"] = None
	settings["timetolive"] = None
	settings["openinterval"] = None
	settings["hashkey"] = None
	
	domRecords = domSettings.getElementsByTagNameNS("http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10", "xmas:flashlist")
	for record in domRecords:
		if record.attributes["name"].nodeValue == qname:
			for collector in record.childNodes:
				if (collector.nodeType == Node.ELEMENT_NODE) and (collector.nodeName == "xmas:flashlist"):
					for attribute in collector.attributes:
						settings[attribute.localName] = collector.attributes[attribute.name]
					break

	"""
	if settings["timeinterval"] == None:
		raise Exception("timeinterval is not defined for the flashlist {0}!".format(fname))

	if settings["flashinterval"] == None:
		raise Exception("flashinterval is not defined for the flashlist {0}!".format(fname))

	if settings["timetolive"] == None:
		raise Exception("timetolive is not defined for the flashlist {0}!".format(fname))
	"""

	json = {}
	templateName = prefix + "-template"
	json[templateName] = {}
	template = json[templateName]
	template["index_patterns"] = [prefix + "_*"]
	template["aliases"] = {zone + "-all": {}, zone + "-" + fname + "-flash": {}, zone + "-" + fname: {}, zone + "-" + tag + "-all": {}, zone + "-" + fname + "-" + tag + "-flash": {}, zone + "-" + fname + "-" + tag: {}}
	template["settings"] = {"index": {"mapper": {"dynamic": "false"}, "store": {"type": settings["indexstoretype"]}, "translog": {"durability": "async"}, "number_of_replicas": settings["numberofreplicas"]}}
	template["mappings"] = {}
	mappings = template["mappings"]

	mappings[fname] = {}
	flashlistType = mappings[fname]
	
	flashlistType["dynamic"] = "strict"

	flashlistType["properties"] = generateJSONProperties(root.childNodes)	
	properties = flashlistType["properties"]
	
	properties["meta_unique_key"] = {"type": "keyword", "store": True, "index": True}
	properties["meta_hash_key"] = {"type": "keyword", "store": True, "index": True}
	properties["flash_key"] = {"type": "keyword", "store": True, "index": True}
	properties["creationtime_"] = {"type": "date", "store": False, "index": True, "format": "epoch_millis"}
	properties["expirationtime_"] = {"type": "date", "store": False, "index": True, "format": "epoch_millis"}
	properties["withdrawtime_"] = {"type": "date", "store": False, "index": True, "format": "epoch_millis"}
	
	flashlistType["_meta"] = {}
	meta = flashlistType["_meta"]
	meta["id"] = id
	meta["version"] = version
	meta["key"] = key
	meta["signature"] = "urn:cmsos:xdaq-timestream"
	meta["zone"] = zone
	meta["tag"] = tag
	meta["version"] = version
	meta["key"] = key
	meta["hashKey"] = settings["hashkey"]
	meta["timetolive"] = settings["timetolive"]
	meta["timeinterval"] = settings["timeinterval"]
	meta["flashinterval"] = settings["flashinterval"]
	if settings["openinterval"] != None:
		meta["openinterval"] = settings["openinterval"]

	meta["definition"] = generateJSONDefinition(root.childNodes)

	return json

def crawlDirectories():
	domSettings = minidom.parse("collector.settings")
	flashlistDir = "flashlists"
	for filename in os.listdir(flashlistDir):
		if filename.endswith(".flash"):
			filePath = os.path.join(flashlistDir, filename)
			domFlashlist = minidom.parse(filePath)
			print(filePath)
			with open(filePath, 'r') as fin:
				print(fin.read())
			document = generateJSONFlashlist(zone, tag, domFlashlist, domSettings)
			print(json.dumps(document, indent = 3, separators = (',', ': ')))

crawlDirectories()

#	flashlists = document.getElementsByTagNameNS("http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10", "flash")
#	for flashlist in flashlists:
#		print flashlist

