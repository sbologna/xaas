import sys
import os
import io
import http.client
import json
from constants import *

if sys.version_info[0] != 3:
	raise Exception("Python 3 is required!")

parameterCount = len(sys.argv)
if parameterCount != 7:
	print("Usage python3 deploy.py <zone> <tag> <filetype> <Elasticsearch URL> <inputDir> <key>")
	exit()

zone = sys.argv[1]
tag = sys.argv[2]
filetype = sys.argv[3]
elasticUrl = sys.argv[4]
inputDir = sys.argv[5]
key = sys.argv[6]

print("zone = '{0}', tag = '{1}', filetype = '{2}', elasticUrl = '{3}', inputDir = '{4}' key='{5}'".format(zone, tag, filetype, elasticUrl, inputDir, key))

#
# Deploy documents into coresponding meta indices
#
def crawlDirectories():
	connection = http.client.HTTPConnection(elasticUrl)
	headers = {'Content-type': 'application/json'}
	indexname = "cmsos-meta-" + zone + "-" + tag + "-" + filetype + "-registry"

	print("Deleting index '" + indexname + "' if it exists"); 	
	connection.request('DELETE', indexname)
	response = connection.getresponse()
	result = response.read().decode()
	print(result)

	#curl -XPUT -H 'Content-Type: application/json'  http://cmsos-iaas-cdaq.cms:9200/cmsos-meta-development-master-animals-registry
	p = {"settings": {"index.mapping.total_fields.limit": 20000, "index.mapping.nested_fields.limit": 10000}}
	payload = json.dumps(p)
	print("Adding index with the following settings: '" + payload + "'") 
	connection.request('PUT', indexname, payload, headers)
	response = connection.getresponse()
	result = response.read().decode()
	print(result)

	#curl -XPUT -H 'Content-Type: application/json' http://cmsos-iaas-cdaq.cms:9200/cmsos-meta-development-master-sensors-registry/_mapping/_doc?pretty -d '{"properties": {"service": {"type": "keyword"}}}'
	p = {"_meta": {"zone": zone, "tag": tag, "signature": signature}, "properties": {key: {"type": "keyword"}}}
	payload = json.dumps(p)
	print("Adding the following index mapping: '" + payload + "'")

	connection.request('POST', indexname + "/_mapping/_doc", payload, headers)
	response = connection.getresponse()
	result = response.read().decode()
	print(result)

	for filename in os.listdir(inputDir):
		if filename.endswith(".json"):
			name = os.path.splitext(filename)[0]
			filePath = os.path.join(inputDir, filename)
			with open(filePath, 'r') as fin:
				document = fin.read()
				connection.request('POST', indexname + "/_doc", document, headers)
				response = connection.getresponse()
				result = response.read().decode()
				print(result)
				resDict = json.loads(result)
				if "error" in resDict.keys():
					sys.exit(1)
				print("Injected document file: {0}".format(filePath))
				
				#conn = http.client.HTTPSConnection("www.python.org")
				#conn.request("GET", "/")
				#r1 = conn.getresponse()
				#print(r1.status, r1.reason)
				
				#connection.request('GET', "/")
				#r1 = connection.getresponse()
				#print(r1.status, r1.reason)
				#data = r1.read()
				#print(data)

crawlDirectories()

