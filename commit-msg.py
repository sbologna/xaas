#!/usr/bin/env python
import sys, re
from subprocess import check_output

commit_msg_filepath = sys.argv[1]
fh = open(commit_msg_filepath, 'r')
commit_msg = fh.read()
fh.close()
commit_msg_regex = '^references\s#(\d+):\s.+'

#branch == "HEAD" in case of a detached head
branch = check_output(['git', 'rev-parse', '--symbolic-full-name', '--abbrev-ref', 'HEAD']).strip()
branch_regex = '^feature_(\d+)(_\w+)?$'

#Ignore in case of a detached head
if (branch != "HEAD") and not re.match(commit_msg_regex, commit_msg):
	print "Declined by commit-msg hook: commit message does not conform to the regex: '" + commit_msg_regex + "'!"
	exit(1)

#In case we are on a feature branch, we can check an issue number too
if re.match(branch_regex, branch):
	issue_from_branch = re.match(branch_regex, branch).group(1)
	issue_from_commit = re.match(commit_msg_regex, commit_msg).group(1)
	#Comparing issue numbers
	if issue_from_commit != issue_from_branch:
		print "Declined by commit-msg hook: issue number in the commit message differ from the one in a branch name: " + issue_from_commit + "<>" + issue_from_branch + "!"
		exit(1)
