changecom()dnl
dnl
define(__SLIMBUSEXTERNALPORT,31002)dnl
define(__BRILBUSEXTERNALPORT,31003)dnl
define(__SPOTLIGHTOCCIEXTERNALPORT,31004)dnl
define(__HEARTBEATEXTERNALPORT,31005)dnl
dnl
define(__INGRESSHOST,`xubernetes.cms')dnl
dnl
define(__PSXREPLICAS,2)dnl
define(__PSXMANAGERNUMBERS,'''252 253''')dnl
define(__PSXDATABASEMANAGERS,`cms-cen-dcs-1-p1 cms-cen-dcs-1-p2')dnl
define(__PSXEVENTMANAGERS,`cms-cen-dcs-1-p1 cms-cen-dcs-1-p2')dnl
define(__PSXENABLED,true)dnl
define(__PSXDNSS,`cmsdimns1,cmsdimns2 cmsdimns1,cmsdimns2')dnl
dnl
define(__SPOTLIGHT2GENABLED,true)dnl
define(__SPOTLIGHTOCCIENABLED,true)dnl
define(__TSTOREENABLED,true)dnl
define(__XMASSTOREENABLED,true)dnl
define(__JOBCONTROLENABLED,true)dnl
define(__XMASADMINENABLED,true)dnl
define(__HEARTBEATENABLED,true)dnl
define(__BRIDGE2GSENTINELENABLED,true)dnl
define(__BRIDGE2GXMASENABLED,true)dnl
dnl
define(__LOCALBUSREPLICAS,1)dnl
define(__SLIMBUSREPLICAS,1)dnl
define(__SLASH2GREPLICAS,1)dnl
dnl
define(__DOCKERIMAGE,gitlab-registry.cern.ch/cmsos/kube/docker-image-cmsos-xdaq-cc7-x64-full:2.23.0.0)dnl
dnl
define(__HYPERDAQPORT,1000)dnl
define(__HYPERDAQPORTNAME,hyperdaq)dnl
dnl
define(__LOCALNETPORT,2000)dnl
define(__LOCALNETPORTNAME,localnet)dnl
dnl
define(__SLIMNETPORT,3000)dnl
define(__SLIMNETPORTNAME,slimnet)dnl
