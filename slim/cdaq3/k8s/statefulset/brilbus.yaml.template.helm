{{- $application := "brilbus" -}}
{{- $profilename := "brilbus" -}}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ $application }}
  namespace: {{ .Values.zone.name }}
spec:
  replicas: 1
  serviceName: {{ $application }}
  selector:
    matchLabels:
      application: {{ $application }}
  template:
    metadata:
      labels:
        application: {{ $application }}
    spec:
      containers:
      - name: {{ $application }}
        image: {{ .Values.zone.dockerimage }}
        imagePullPolicy: Always
        ports:
        - containerPort: {{ .Values.zone.network.hyperdaq.port }}
        - containerPort: {{ .Values.zone.network.slimnet.port }} 
        readinessProbe:
        {{- include "readiness.probe" . | nindent 10 }}
        env:
        {{- include "application.env" . | nindent 8 }}
        command: ["/bin/bash", "-c"]
        args: 
        - profile=$(mktemp);
          export INSTANCE=${HOSTNAME#{{ $profilename }}-};
          envsubst < /opt/xdaq/share/{{ .Values.zone.name }}/profile/{{ $profilename }}.profile > $profile;
          echo "$POD_IP $HOSTNAME.{{ $profilename }}" >> /etc/hosts;
        {{- include "localbus.ready" . | nindent 10 }}
        {{- include "application.args" . | nindent 10 }}
        volumeMounts:
        - name: profile-volume
          mountPath: /opt/xdaq/share/{{ .Values.zone.name }}/profile
        - name: sensor-volume
          mountPath: /opt/xdaq/share/{{ .Values.zone.name }}/sensor
        - name: flash-volume
          mountPath: /opt/xdaq/share/{{ .Values.zone.name }}/flash
      volumes:
      - name: profile-volume
        configMap:
          name: {{ $application }}
      - name: sensor-volume
        configMap:
          name: sensor
      - name: flash-volume
        configMap:
          name: flash
