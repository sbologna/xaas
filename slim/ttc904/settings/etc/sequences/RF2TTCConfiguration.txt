###########################################################
#
# RF2TTC configuration.
#
# (from RF2TTCControl::WriteConfigurationFile())
#
# Authors: Tim Christiansen, Emlyn Corrin and
# Andre Holzner (current contact) 2005-2007
#
# Automatically created with RF2TTC::WriteConfiguration() 
# from current configuration on Thu Aug 21 18:09:26 2008 
# 
# (Comments (will be ignored) are indicated by "#". Lines
#  can be split using the '\' character at end of 1st line.)
# 
###########################################################

########################################
# Channel BC1
########################################

# Input DAC value (0..255)
INPUTDAC CHANNEL=BC1 COUNTS=128

# sources for various operating modes
#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=EXTERNAL for external clock input
SIGNAL_SELECT CHANNEL=BC1 SOURCE=EXTERNAL MODE=MANUAL

#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=EXTERNAL for external clock input
SIGNAL_SELECT CHANNEL=BC1 SOURCE=EXTERNAL MODE=BEAM

#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=EXTERNAL for external clock input
SIGNAL_SELECT CHANNEL=BC1 SOURCE=INTERNAL MODE=NOBEAM

# Delay25 on/off and delay value
DELAY25 CHANNEL=BC1 ENABLE=YES STEPS=0
# QPLL mode (auto or manual reset)
QPLLMODE CHANNEL=BC1 RELOCKMODE=AUTO

# Working mode for this channel: Manual or automatic (i.e. depending on the LHC mode)
WORKING_MODE CHANNEL=BC1 MODE=MANUAL

########################################
# Channel BC2
########################################

# Input DAC value (0..255)
INPUTDAC CHANNEL=BC2 COUNTS=128

# sources for various operating modes
#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=EXTERNAL for external clock input
SIGNAL_SELECT CHANNEL=BC2 SOURCE=EXTERNAL MODE=MANUAL

#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=EXTERNAL for external clock input
SIGNAL_SELECT CHANNEL=BC2 SOURCE=EXTERNAL MODE=BEAM

#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=EXTERNAL for external clock input
SIGNAL_SELECT CHANNEL=BC2 SOURCE=INTERNAL MODE=NOBEAM

# Delay25 on/off and delay value
DELAY25 CHANNEL=BC2 ENABLE=YES STEPS=0
# QPLL mode (auto or manual reset)
QPLLMODE CHANNEL=BC2 RELOCKMODE=AUTO

# Working mode for this channel: Manual or automatic (i.e. depending on the LHC mode)
WORKING_MODE CHANNEL=BC2 MODE=MANUAL

########################################
# Channel BCREF
########################################

# Input DAC value (0..255)
INPUTDAC CHANNEL=BCREF COUNTS=128

# sources for various operating modes
#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=EXTERNAL for external clock input
SIGNAL_SELECT CHANNEL=BCREF SOURCE=EXTERNAL MODE=MANUAL

#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=EXTERNAL for external clock input
SIGNAL_SELECT CHANNEL=BCREF SOURCE=EXTERNAL MODE=BEAM

#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=EXTERNAL for external clock input
SIGNAL_SELECT CHANNEL=BCREF SOURCE=INTERNAL MODE=NOBEAM

# Delay25 on/off and delay value
DELAY25 CHANNEL=BCREF ENABLE=YES STEPS=0
# QPLL mode (auto or manual reset)
QPLLMODE CHANNEL=BCREF RELOCKMODE=AUTO

# Working mode for this channel: Manual or automatic (i.e. depending on the LHC mode)
WORKING_MODE CHANNEL=BCREF MODE=MANUAL

########################################
# Channel BCMAIN
########################################

# sources for various operating modes
#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=BC1,BC2 or BCREF for the corresponding external clock input
SIGNAL_SELECT CHANNEL=BCMAIN SOURCE=INTERNAL MODE=MANUAL

#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=BC1,BC2 or BCREF for the corresponding external clock input
SIGNAL_SELECT CHANNEL=BCMAIN SOURCE=BCREF MODE=BEAM

#   Specify SOURCE=INTERNAL for internal clock
#   Specify SOURCE=BC1,BC2 or BCREF for the corresponding external clock input
SIGNAL_SELECT CHANNEL=BCMAIN SOURCE=INTERNAL MODE=NOBEAM

# Delay25 on/off and delay value
DELAY25 CHANNEL=BCMAIN ENABLE=YES STEPS=0
# QPLL mode (auto or manual reset)
QPLLMODE CHANNEL=BCMAIN RELOCKMODE=AUTO

# Working mode for this channel: Manual or automatic (i.e. depending on the LHC mode)
WORKING_MODE CHANNEL=BCMAIN MODE=MANUAL

########################################
# Channel Orbit 1
########################################

# Input DAC value (0..255)
INPUTDAC CHANNEL=ORBIT1 COUNTS=170

# Delay25 at input: on/off and delay value
DELAY25 CHANNEL=ORBIT1_IN ENABLE=YES STEPS=0
# Delay25 at output: on/off and delay value
DELAY25 CHANNEL=ORBIT1_OUT ENABLE=YES STEPS=0
# internal orbit generator period and enable status
INTERNAL_ORBIT CHANNEL=ORBIT1 PERIOD=3564 ENABLE=YES

# Orbit coarse delay in number of bunch crossings
ORBIT_COARSE_DELAY CHANNEL=ORBIT1 NUMBX=20
# sources for various operating modes
#   Specify SOURCE=INTERNAL for internal orbit
#   Specify SOURCE=EXTERNAL for external orbit input
SIGNAL_SELECT CHANNEL=ORBIT1 SOURCE=EXTERNAL MODE=MANUAL

#   Specify SOURCE=INTERNAL for internal orbit
#   Specify SOURCE=EXTERNAL for external orbit input
SIGNAL_SELECT CHANNEL=ORBIT1 SOURCE=EXTERNAL MODE=BEAM

#   Specify SOURCE=INTERNAL for internal orbit
#   Specify SOURCE=EXTERNAL for external orbit input
SIGNAL_SELECT CHANNEL=ORBIT1 SOURCE=INTERNAL MODE=NOBEAM

# Working mode for this channel: Manual or automatic (i.e. depending on the LHC mode)
WORKING_MODE CHANNEL=ORBIT1 MODE=MANUAL

########################################
# Channel Orbit 2
########################################

# Input DAC value (0..255)
INPUTDAC CHANNEL=ORBIT2 COUNTS=170

# Delay25 at input: on/off and delay value
DELAY25 CHANNEL=ORBIT2_IN ENABLE=YES STEPS=0
# Delay25 at output: on/off and delay value
DELAY25 CHANNEL=ORBIT2_OUT ENABLE=YES STEPS=0
# internal orbit generator period and enable status
INTERNAL_ORBIT CHANNEL=ORBIT2 PERIOD=3564 ENABLE=YES

# Orbit coarse delay in number of bunch crossings
ORBIT_COARSE_DELAY CHANNEL=ORBIT2 NUMBX=70
# sources for various operating modes
#   Specify SOURCE=INTERNAL for internal orbit
#   Specify SOURCE=EXTERNAL for external orbit input
SIGNAL_SELECT CHANNEL=ORBIT2 SOURCE=EXTERNAL MODE=MANUAL

#   Specify SOURCE=INTERNAL for internal orbit
#   Specify SOURCE=EXTERNAL for external orbit input
SIGNAL_SELECT CHANNEL=ORBIT2 SOURCE=EXTERNAL MODE=BEAM

#   Specify SOURCE=INTERNAL for internal orbit
#   Specify SOURCE=EXTERNAL for external orbit input
SIGNAL_SELECT CHANNEL=ORBIT2 SOURCE=INTERNAL MODE=NOBEAM

# Working mode for this channel: Manual or automatic (i.e. depending on the LHC mode)
WORKING_MODE CHANNEL=ORBIT2 MODE=MANUAL

########################################
# Channel Main Orbit
########################################

# Delay25 at output: on/off and delay value
DELAY25 CHANNEL=ORBITMAIN_OUT ENABLE=YES STEPS=10
# internal orbit generator period and enable status
INTERNAL_ORBIT CHANNEL=ORBITMAIN PERIOD=3564 ENABLE=YES

# Orbit coarse delay in number of bunch crossings
ORBIT_COARSE_DELAY CHANNEL=ORBITMAIN NUMBX=60
# sources for various operating modes
#   Specify SOURCE=INTERNAL for internal orbit
#   Specify SOURCE=ORBIT1 or ORBIT1 for the corresponding external orbit input
SIGNAL_SELECT CHANNEL=ORBITMAIN SOURCE=INTERNAL MODE=MANUAL

#   Specify SOURCE=INTERNAL for internal orbit
#   Specify SOURCE=ORBIT1 or ORBIT1 for the corresponding external orbit input
SIGNAL_SELECT CHANNEL=ORBITMAIN SOURCE=ORBIT1 MODE=BEAM

#   Specify SOURCE=INTERNAL for internal orbit
#   Specify SOURCE=ORBIT1 or ORBIT1 for the corresponding external orbit input
SIGNAL_SELECT CHANNEL=ORBITMAIN SOURCE=INTERNAL MODE=NOBEAM

# Working mode for this channel: Manual or automatic (i.e. depending on the LHC mode)
WORKING_MODE CHANNEL=ORBITMAIN MODE=MANUAL

# beam / nobeam definitions: For channels which are in automatic mode,
# this determines whether they should use the 'beam' or 'nobeam' configuration
# for each possible LHC machine mode.
BEAM_NO_BEAM_DEF MODE="LHC mode 0" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="No mode, data is not available, not set" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Setup" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Pilot injection" CONFIG=BEAM
BEAM_NO_BEAM_DEF MODE="Intermediate injection" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Nominal injection" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Before ramp" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Ramp" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Flat top" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Squeeze" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Adjust beam on flat top" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Stable beam for physics" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Unstable beam" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Beam dump" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Ramp down" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Recovering" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Inject and dump" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Circulate and dump" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Recovery after a beam permit flag drop" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Pre-cycle before injection, no beam" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="Warning beam dump" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="No beam or preparation for beam" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="LHC mode 22" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="LHC mode 23" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="LHC mode 24" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="LHC mode 25" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="LHC mode 26" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="LHC mode 27" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="LHC mode 28" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="LHC mode 29" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="LHC mode 30" CONFIG=NOBEAM
BEAM_NO_BEAM_DEF MODE="LHC mode 31" CONFIG=NOBEAM
