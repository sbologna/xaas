#include "extension/settings.include"
<?xml version='1.0'?>

<xc:Partition
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
  xmlns:xc="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30">

  <!-- ======================================== -->
  <!-- TStore                                   -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,SLIM_MACRO_TSTORE_HTTP_PORT))>
    <xc:Application
      class="tstore::TStore"
      id="120"
      instance="0"
      network="local"
      group="ttc"
      service="tstore"
      publish="true">
    </xc:Application>
  </xc:Context>

  <!-- ======================================== -->
  <!-- Frequency Meter XDAQ application         -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,1971))>
    <xc:Application
      class="ttc::FreqMeterControl"
      id="49"
      instance="1000"
      network="local"
      group="ttc"
      publish="true"
      service="ttc-mi-freqmeter">
      <properties
        xmlns="urn:xdaq-application:ttc::FreqMeterControl"
        xsi:type="soapenc:Struct">
        <system xsi:type="xsd:string">dummy</system>
        <FreqMeterURL xsi:type="xsd:string">freqmeter1.cms</FreqMeterURL>
        <FreqMeterPort xsi:type="xsd:unsignedLong">4001</FreqMeterPort>
        <FreqMeterRefSrc xsi:type="xsd:string">external</FreqMeterRefSrc>
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcmonitoring.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcfreqmon.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- RF2TTC XDAQ application                  -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,1972))>
    <xc:Application
      class="ttc::RF2TTCControl"
      id="50"
      instance="1000"
      network="local"
      group="ttc"
      publish="true"
      service="ttc-mi-rf2ttc">
      <properties
        xmlns="urn:xdaq-application:ttc::RF2TTCControl"
        xsi:type="soapenc:Struct">
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">RF2TTC-9-dummy</name>
        <BusAdapter xsi:type="xsd:string">DUMMY64X:0</BusAdapter>
        <Location xsi:type="xsd:integer">9</Location>
        <Configuration xsi:type="xsd:string">[file=${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}/etc/sequences/RF2TTCConfiguration.txt]</Configuration>
        <YuiBaseUrl xsi:type="xsd:string">/yui/build/</YuiBaseUrl>
        <DisableVMEWrite xsi:type="xsd:boolean">false</DisableVMEWrite>
        <!-- DisableMonitoring xsi:type="xsd:boolean">true</DisableMonitoring -->
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcmonitoring.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcrf2ttc.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- RFRXD XDAQ applications                  -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,1973))>
    <!-- The channel assignments are documented at: https://twiki.cern.ch/twiki/bin/view/CMS/TTCManualBSTAndTTCFibres. -->

    <!-- RFRXDControl for beam 1 -->
    <xc:Application
      class="ttc::RFRXDControl"
      id="60"
      instance="5001"
      network="local"
      publish="true"
      service="ttc-mi-rfrxd">
      <properties
        xmlns="urn:xdaq-application:ttc::RFRXDControl"
        xsi:type="soapenc:Struct">
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">RFRXD-13-beam1-dummy</name>
        <BusAdapter xsi:type="xsd:string">DUMMY64X:0</BusAdapter>
        <!-- NOTE: The RFRXD firmware uses only four bits for addressing, so it does not work in slots > 15. -->
        <Location xsi:type="xsd:integer">13</Location>
        <!-- Channel 1 on the RFRXD for beam 1 is BCREF. -->
        <!-- Channel 2 on the RFRXD for beam 1 is BC1. -->
        <!-- Channel 3 on the RFRXD for beam 1 is ORBIT1. -->
        <Channel1SignalName xsi:type="xsd:string">BCREF</Channel1SignalName>
        <Channel2SignalName xsi:type="xsd:string">BC1</Channel2SignalName>
        <Channel3SignalName xsi:type="xsd:string">ORBIT1</Channel3SignalName>
        <Channel1DACValue xsi:type="xsd:unsignedInt">9</Channel1DACValue>
        <Channel2DACValue xsi:type="xsd:unsignedInt">9</Channel2DACValue>
        <Channel3DACValue xsi:type="xsd:unsignedInt">160</Channel3DACValue>
        <!-- names of the corresponding datapoints in PVSS for monitoring the frequency -->
        <Channel1FrequencyDPName xsi:type="xsd:string">cms_cen_dcs_2:RFRXInfo.F40_REF</Channel1FrequencyDPName>
        <Channel2FrequencyDPName xsi:type="xsd:string">cms_cen_dcs_2:RFRXInfo.F40_B1</Channel2FrequencyDPName>
        <Channel3FrequencyDPName xsi:type="xsd:string">cms_cen_dcs_2:RFRXInfo.FREV_B1</Channel3FrequencyDPName>
      </properties>
    </xc:Application>

    <!-- RFRXDControl for beam 2 -->
    <xc:Application
      class="ttc::RFRXDControl"
      id="61"
      instance="5002"
      network="local"
      publish="true"
      service="ttc-mi-rfrxd">
      <properties
        xmlns="urn:xdaq-application:ttc::RFRXDControl"
        xsi:type="soapenc:Struct">
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">RFRXD-14-beam2-dummy</name>
        <BusAdapter xsi:type="xsd:string">DUMMY64X:0</BusAdapter>
        <!-- NOTE: The RFRXD firmware uses only four bits for addressing, so it does not work in slots > 15. -->
        <Location xsi:type="xsd:integer">14</Location>
        <!-- Channel 1 on the RFRXD for beam 2 is not connected. -->
        <!-- Channel 2 on the RFRXD for beam 2 is BC2. -->
        <!-- Channel 3 on the RFRXD for beam 2 is ORBIT2. -->
        <Channel1SignalName xsi:type="xsd:string"></Channel1SignalName>
        <Channel2SignalName xsi:type="xsd:string">BC2</Channel2SignalName>
        <Channel3SignalName xsi:type="xsd:string">ORBIT2</Channel3SignalName>
        <Channel1DACValue xsi:type="xsd:unsignedInt">160</Channel1DACValue>
        <Channel2DACValue xsi:type="xsd:unsignedInt">9</Channel2DACValue>
        <Channel3DACValue xsi:type="xsd:unsignedInt">160</Channel3DACValue>
        <!-- names of the corresponding datapoints in PVSS for monitoring the frequency -->
        <Channel2FrequencyDPName xsi:type="xsd:string">cms_cen_dcs_2:RFRXInfo.F40_B2</Channel2FrequencyDPName>
        <Channel3FrequencyDPName xsi:type="xsd:string">cms_cen_dcs_2:RFRXInfo.FREV_B2</Channel3FrequencyDPName>
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcrfrxd.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- BOBR                                     -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,1974))>
    <xc:Application
      class="ttc::BOBRControl"
      id="60"
      instance="5000"
      network="local"
      service="ttc-mi-bobr">
      <properties
        xmlns="urn:xdaq-application:ttc::BOBRControl"
        xsi:type="soapenc:Struct">
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">BOBR-dummy</name>
        <BusAdapter xsi:type="xsd:string">DUMMY64X:0</BusAdapter>
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcbobr.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- LTC                                      -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,8000))>
    <xc:Application
      class="ttc::LTCControl"
      id="30"
      instance="14"
      network="local">
      <properties
        xmlns="urn:xdaq-application:ttc::LTCControl"
        xsi:type="soapenc:Struct">
        <group xsi:type="xsd:string">TTC</group>
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">LTC-2-dummy</name>
        <BusAdapter xsi:type="xsd:string">DUMMY64X:0</BusAdapter>
        <Location xsi:type="xsd:integer">2</Location>
        <Configuration xsi:type="xsd:string">[file=/opt/xdaq/share/${XDAQ_ZONE}/etc/sequences/test-ltc.txt]</Configuration>
        <BTimeCorrection xsi:type="xsd:unsignedInt">99</BTimeCorrection>
        <SlinkSrcId xsi:type="xsd:unsignedInt">815</SlinkSrcId>
      </properties>
    </xc:Application>

    <xc:Application
      class="ttc::LTCControl"
      id="31"
      instance="15"
      network="local">
      <properties
        xmlns="urn:xdaq-application:ttc::LTCControl"
        xsi:type="soapenc:Struct">
        <group xsi:type="xsd:string">TTC</group>
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">LTC-3-dummy</name>
        <BusAdapter xsi:type="xsd:string">DUMMY64X:0</BusAdapter>
        <Location xsi:type="xsd:integer">3</Location>
        <Configuration xsi:type="xsd:string">[file=/opt/xdaq/share/${XDAQ_ZONE}/etc/sequences/test-ltc.txt]</Configuration>
        <BTimeCorrection xsi:type="xsd:unsignedInt">99</BTimeCorrection>
        <SlinkSrcId xsi:type="xsd:unsignedInt">815</SlinkSrcId>
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcltc.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- TTCci slot 8 (RCT)                       -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,8999))>
    <xc:Application
      class="ttc::TTCciControl"
      id="40"
      instance="13"
      network="local"
      group="ttc"
      publish="true"
      service="ttc-ttcci">
      <properties
        xmlns="urn:xdaq-application:ttc::TTCciControl"
        xsi:type="soapenc:Struct">
        <group xsi:type="xsd:string">TTC</group>
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">TTCci-8-RCT</name>
        <BusAdapter xsi:type="xsd:string">CAENPCI:0</BusAdapter>
        <Location xsi:type="xsd:integer">8</Location>
        <Configuration xsi:type="xsd:string">[file=/opt/xdaq/share/${XDAQ_ZONE}/etc/sequences/ttc-904-TTCciConfiguration-RCT-default_2012.txt]</Configuration>
        <BTimeCorrection xsi:type="xsd:unsignedInt">100</BTimeCorrection>
        <DelayT2Correction xsi:type="xsd:unsignedInt">3</DelayT2Correction>
        <!-- DisableMonitoring xsi:type="xsd:boolean">true</DisableMonitoring -->
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcmonitoring.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcttcci.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- TTCci slot 9 (BRIL)                      -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,9000))>
    <xc:Application
      class="ttc::TTCciControl"
      id="40"
      instance="13"
      network="local"
      group="ttc"
      publish="true"
      service="ttc-ttcci">
      <properties
        xmlns="urn:xdaq-application:ttc::TTCciControl"
        xsi:type="soapenc:Struct">
        <group xsi:type="xsd:string">TTC</group>
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">TTCci-9-BRIL</name>
        <BusAdapter xsi:type="xsd:string">CAENPCI:0</BusAdapter>
        <Location xsi:type="xsd:integer">9</Location>
        <Configuration xsi:type="xsd:string">[file=/opt/xdaq/share/${XDAQ_ZONE}/etc/sequences/ttc-904-TTCciConfiguration-BRIL.txt]</Configuration>
        <BTimeCorrection xsi:type="xsd:unsignedInt">100</BTimeCorrection>
        <DelayT2Correction xsi:type="xsd:unsignedInt">3</DelayT2Correction>
        <!-- DisableMonitoring xsi:type="xsd:boolean">true</DisableMonitoring -->
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcmonitoring.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcttcci.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- TTCci slot 10 (GCT)                      -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,9001))>
    <xc:Application
      class="ttc::TTCciControl"
      id="40"
      instance="13"
      network="local"
      group="ttc"
      publish="true"
      service="ttc-ttcci">
      <properties
        xmlns="urn:xdaq-application:ttc::TTCciControl"
        xsi:type="soapenc:Struct">
        <group xsi:type="xsd:string">TTC</group>
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">TTCci-10-GCT</name>
        <BusAdapter xsi:type="xsd:string">CAENPCI:0</BusAdapter>
        <Location xsi:type="xsd:integer">10</Location>
        <Configuration xsi:type="xsd:string">[file=/opt/xdaq/share/${XDAQ_ZONE}/etc/sequences/ttc-904-TTCciConfiguration-GCT.txt]</Configuration>
        <BTimeCorrection xsi:type="xsd:unsignedInt">100</BTimeCorrection>
        <DelayT2Correction xsi:type="xsd:unsignedInt">3</DelayT2Correction>
        <!-- DisableMonitoring xsi:type="xsd:boolean">true</DisableMonitoring -->
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcmonitoring.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcttcci.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- TTCci slot 11 (RPC)                      -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,9002))>
    <xc:Application
      class="ttc::TTCciControl"
      id="40"
      instance="13"
      network="local"
      group="ttc"
      publish="true"
      service="ttc-ttcci">
      <properties
        xmlns="urn:xdaq-application:ttc::TTCciControl"
        xsi:type="soapenc:Struct">
        <group xsi:type="xsd:string">TTC</group>
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">TTCci-11-RPC</name>
        <BusAdapter xsi:type="xsd:string">CAENPCI:0</BusAdapter>
        <Location xsi:type="xsd:integer">11</Location>
        <Configuration xsi:type="xsd:string">[file=/opt/xdaq/share/${XDAQ_ZONE}/etc/sequences/ttc-904-TTCciConfiguration-RPC.txt]</Configuration>
        <BTimeCorrection xsi:type="xsd:unsignedInt">100</BTimeCorrection>
        <DelayT2Correction xsi:type="xsd:unsignedInt">3</DelayT2Correction>
        <!-- DisableMonitoring xsi:type="xsd:boolean">true</DisableMonitoring -->
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcmonitoring.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcttcci.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- TTCci slot 12 (DT)                       -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,9003))>
    <xc:Application
      class="ttc::TTCciControl"
      id="40"
      instance="13"
      network="local"
      group="ttc"
      publish="true"
      service="ttc-ttcci">
      <properties
        xmlns="urn:xdaq-application:ttc::TTCciControl"
        xsi:type="soapenc:Struct">
        <group xsi:type="xsd:string">TTC</group>
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">TTCci-12-DT</name>
        <BusAdapter xsi:type="xsd:string">CAENPCI:0</BusAdapter>
        <Location xsi:type="xsd:integer">12</Location>
        <Configuration xsi:type="xsd:string">[file=/opt/xdaq/share/${XDAQ_ZONE}/etc/sequences/ttc-904-TTCciConfiguration-DT.txt]</Configuration>
        <BTimeCorrection xsi:type="xsd:unsignedInt">100</BTimeCorrection>
        <DelayT2Correction xsi:type="xsd:unsignedInt">3</DelayT2Correction>
        <!-- DisableMonitoring xsi:type="xsd:boolean">true</DisableMonitoring -->
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcmonitoring.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcttcci.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- TTCci slot 16 (HCAL)                     -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,9007))>
    <xc:Application
      class="ttc::TTCciControl"
      id="40"
      instance="13"
      network="local"
      group="ttc"
      publish="true"
      service="ttc-ttcci">
      <properties
        xmlns="urn:xdaq-application:ttc::TTCciControl"
        xsi:type="soapenc:Struct">
        <group xsi:type="xsd:string">TTC</group>
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">TTCci-16-HCAL</name>
        <BusAdapter xsi:type="xsd:string">CAENPCI:0</BusAdapter>
        <Location xsi:type="xsd:integer">16</Location>
        <Configuration xsi:type="xsd:string">[file=/opt/xdaq/share/${XDAQ_ZONE}/etc/sequences/ttc-904-TTCciConfiguration-HCAL.txt]</Configuration>
        <BTimeCorrection xsi:type="xsd:unsignedInt">100</BTimeCorrection>
        <DelayT2Correction xsi:type="xsd:unsignedInt">3</DelayT2Correction>
        <!-- DisableMonitoring xsi:type="xsd:boolean">true</DisableMonitoring -->
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcmonitoring.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcttcci.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- TTCci slot 17 (EE)                       -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,9008))>
    <xc:Application
      class="ttc::TTCciControl"
      id="40"
      instance="13"
      network="local"
      group="ttc"
      publish="true"
      service="ttc-ttcci">
      <properties
        xmlns="urn:xdaq-application:ttc::TTCciControl"
        xsi:type="soapenc:Struct">
        <group xsi:type="xsd:string">TTC</group>
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">TTCci-17-EE</name>
        <BusAdapter xsi:type="xsd:string">CAENPCI:0</BusAdapter>
        <Location xsi:type="xsd:integer">17</Location>
        <Configuration xsi:type="xsd:string">[file=/opt/xdaq/share/${XDAQ_ZONE}/etc/sequences/ttc-904-TTCciConfiguration-ECAL.txt]</Configuration>
        <BTimeCorrection xsi:type="xsd:unsignedInt">100</BTimeCorrection>
        <DelayT2Correction xsi:type="xsd:unsignedInt">3</DelayT2Correction>
        <!-- DisableMonitoring xsi:type="xsd:boolean">true</DisableMonitoring -->
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcmonitoring.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcttcci.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- TTCci slot 18 (EB)                       -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,9009))>
    <xc:Application
      class="ttc::TTCciControl"
      id="40"
      instance="13"
      network="local"
      group="ttc"
      publish="true"
      service="ttc-ttcci">
      <properties
        xmlns="urn:xdaq-application:ttc::TTCciControl"
        xsi:type="soapenc:Struct">
        <group xsi:type="xsd:string">TTC</group>
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">TTCci-18-EB</name>
        <BusAdapter xsi:type="xsd:string">CAENPCI:0</BusAdapter>
        <Location xsi:type="xsd:integer">18</Location>
        <Configuration xsi:type="xsd:string">[file=/opt/xdaq/share/${XDAQ_ZONE}/etc/sequences/ttc-904-TTCciConfiguration-ECAL.txt]</Configuration>
        <BTimeCorrection xsi:type="xsd:unsignedInt">100</BTimeCorrection>
        <DelayT2Correction xsi:type="xsd:unsignedInt">3</DelayT2Correction>
        <!-- DisableMonitoring xsi:type="xsd:boolean">true</DisableMonitoring -->
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcmonitoring.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcttcci.so</xc:Module>
  </xc:Context>

  <!-- ======================================== -->
  <!-- TTCci slot 19 (ES)                       -->
  <!-- ======================================== -->
  <xc:Context url=SLIM_MACRO_TOSTR(SLIM_MACRO_TOURL(http,SLIM_MACRO_SERVICE_HOST,9010))>
    <xc:Application
      class="ttc::TTCciControl"
      id="40"
      instance="13"
      network="local"
      group="ttc"
      publish="true"
      service="ttc-ttcci">
      <properties
        xmlns="urn:xdaq-application:ttc::TTCciControl"
        xsi:type="soapenc:Struct">
        <group xsi:type="xsd:string">TTC</group>
        <system xsi:type="xsd:string">904</system>
        <name xsi:type="xsd:string">TTCci-19-ES</name>
        <BusAdapter xsi:type="xsd:string">CAENPCI:0</BusAdapter>
        <Location xsi:type="xsd:integer">19</Location>
        <Configuration xsi:type="xsd:string">[file=/opt/xdaq/share/${XDAQ_ZONE}/etc/sequences/ttc-904-TTCciConfiguration-ES.txt]</Configuration>
        <BTimeCorrection xsi:type="xsd:unsignedInt">100</BTimeCorrection>
        <DelayT2Correction xsi:type="xsd:unsignedInt">3</DelayT2Correction>
        <!-- DisableMonitoring xsi:type="xsd:boolean">true</DisableMonitoring -->
      </properties>
    </xc:Application>
    <xc:Module>/opt/xdaq/lib/libttcutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcmonitoring.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libttcttcci.so</xc:Module>
  </xc:Context>

</xc:Partition>
