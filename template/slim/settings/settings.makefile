# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2020, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleber, L. Orsini and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# 
# 
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../..

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

PackageName=$(ZONE_NAME)-settings
Project=$(PROJECT_NAME)
Package=slim/$(ZONE_NAME)/settings



Summary=Setting for $(ZONE_NAME) setup 

Description=This is a configuration (flashlist and other settings) for $(ZONE_NAME) 

Link=http://xdaq.web.cern.ch
#
# Template instantiate value, as per zone
#
TEMPLATEDIR=$(XAAS_ROOT)/template/slim
SLIM_SERVICE_HOST=$(shell cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(XAAS_ROOT)/template -I$(TEMPLATEDIR)/ -DSLIM_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/defaultvar.macros | grep SLIM_MACRO_SERVICE_HOST | awk '{print $$2}')
SLIM_DIRECTORY_SERVICE_PORT=$(shell cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DSLIM_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/defaultvar.macros | grep SLIM_MACRO_DIRECTORY_SERVICE_HTTP_PORT | awk '{print $$2}')

_all: all

default: all

all: clean
	mkdir -p sensor
	cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(XAAS_ROOT)/template -I$(TEMPLATEDIR)/ -DSLIM_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/defaultvar.macros > etc/defaultvar.txt
	cp $(TEMPLATEDIR)/settings/flash/* flash 
	cp $(TEMPLATEDIR)/settings/sensor/*.sensor sensor
	cp $(TEMPLATEDIR)/settings/sensor/*.pulser sensor
ifeq ($(findstring xmas-slash2g.settings,$(wildcard sensor.template/xmas-slash2g.settings)), xmas-slash2g.settings)
	cp sensor.template/xmas-slash2g.settings sensor
else
	cp $(TEMPLATEDIR)/settings/sensor/xmas-slash2g.settings sensor
endif
	if [ -e $(BUILD_HOME)/$(Package)/etc/tracerfilter.xml ]; then \
		echo "Using user tracer filter"; \
	else \
		cp $(TEMPLATEDIR)/settings/etc/tracerfilter.xml $(BUILD_HOME)/$(Package)/etc/; \
	fi
	cd $(BUILD_HOME)/$(Package)/sensor.template; find . -maxdepth 1 \( -type f -o -type l \) -exec cp {} $(BUILD_HOME)/$(Package)/sensor \;
	find $(BUILD_HOME)/$(Package)/sensor/ $(BUILD_HOME)/$(Package)/flash/ -maxdepth 1 -type f -exec bash -c 'tmpfile=$$(mktemp); cpp -I$(BUILD_HOME)/$(Package)/../ -I$(XAAS_ROOT)/template -I$(TEMPLATEDIR)/ -P {} $${tmpfile}; mv $${tmpfile} {};' \;
	cp $(TEMPLATEDIR)/settings/spec.template .
	perl -p -i -e 's#__zonename__#$(ZONE_NAME)#' spec.template
	sed -i 's/$${SLIM_SERVICE_HOST}/$(SLIM_SERVICE_HOST)/g' sensor/*.sensor 
	sed -i 's/$${SLIM_SERVICE_HOST}/$(SLIM_SERVICE_HOST)/g' sensor/*.pulser
	sed -i 's/$${SLIM_SERVICE_HOST}/$(SLIM_SERVICE_HOST)/g' sensor/*.settings
	sed -i 's/$${SLIM_DIRECTORY_SERVICE_PORT}/$(SLIM_DIRECTORY_SERVICE_PORT)/g' sensor/*.sensor
	sed -i 's/$${SLIM_DIRECTORY_SERVICE_PORT}/$(SLIM_DIRECTORY_SERVICE_PORT)/g' sensor/*.pulser
	sed -i 's/$${SLIM_DIRECTORY_SERVICE_PORT}/$(SLIM_DIRECTORY_SERVICE_PORT)/g' sensor/*.settings
	sed -i 's/@@/\/\//g' sensor/*.sensor
	sed -i 's/@@/\/\//g' sensor/*.pulser
	sed -i 's/@@/\/\//g' sensor/*.settings

_cleanall: clean

clean:
	-rm -rf sensor
	-rm -f spec.template
	-rm -f etc/tracerfilter.xml
	-rm -f etc/defaultvar.txt

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfSetupRPM.rules
