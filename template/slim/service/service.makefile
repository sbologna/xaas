# $Id: Makefile,v 1.110 2009/05/29 13:15:07 rmoser Exp $

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2004, CERN.			                #
# All rights reserved.                                                  #
# Authors: J. Gutleber and L. Orsini					#
#                                                                       #
# For the licensing terms see LICENSE.		                        #
# For the list of contributors see CREDITS.   			        #
#########################################################################

##
#
# 
# 
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../..

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

PackageName=$(ZONE_NAME)-service
Project=$(PROJECT_NAME)
Package=slim/$(ZONE_NAME)/service

Summary=XAAS configuration for $(ZONE_NAME) service

Description=This package provides XAAS configuration for service on zone $(ZONE_NAME)

Link=http://xdaq.web.cern.ch
#
# Template instantiate value
#
TEMPLATEDIR=$(XAAS_ROOT)/template/slim
SLIM_SERVICE_HOST=$(shell cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(XAAS_ROOT)/template -I$(TEMPLATEDIR)/ -DSLIM_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/defaultvar.macros | grep SLIM_MACRO_SERVICE_HOST | awk -F' ' '{print $$2}')
SLIM_BRIDGE2G_SENTINEL_PORT=$(shell cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DSLIM_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/defaultvar.macros | grep SLIM_MACRO_BRIDGE2G_SENTINEL_HTTP_PORT | awk -F' ' '{print $$2}')

_all: all

default: all

all: clean
	mkdir -p profile
	mkdir -p conf 
	mkdir -p scope
	echo "service"  > scope/service.scope
	cp $(TEMPLATEDIR)/service/spec.template .
	perl -p -i -e 's#__zonename__#$(ZONE_NAME)#' spec.template
	if [ -e hosts.setup.template ]; then \
		cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DSLIM_MACRO_ZONE=$(ZONE_NAME) hosts.setup.template hosts.setup; \
	else \
		cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DSLIM_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/hosts.setup.template hosts.setup; \
	fi
	cp $(TEMPLATEDIR)/service/conf/*.conf.in conf
	cp $(TEMPLATEDIR)/service/conf/htaccess conf
	cp $(TEMPLATEDIR)/service/conf/htgroup conf
	cp $(TEMPLATEDIR)/service/conf/htpasswd conf
	find $(TEMPLATEDIR)/service/profile/ -type f -name "*.profile.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} profile/$${filename%.template}' \;
	find $(TEMPLATEDIR)/service/conf/ -type f -name "*.conf.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} conf/$${filename%.template}' \;
	find $(BUILD_HOME)/$(Package)/profile/ -type f -name "*.profile.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} profile/$${filename%.template}' \;
	find $(BUILD_HOME)/$(Package)/conf/ -type f -name "*.conf.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} conf/$${filename%.template}' \;
	cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DSLIM_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/settings.xml.template settings.xml
	sed -i 's/@@/\/\//g' profile/*.profile 
	sed -i 's/@@/\/\//g' conf/*.conf
	sed -i 's/@@/\/\//g' settings.xml
	sed -i 's/@@/\/\//g' hosts.setup
	mkdir -p system
	mkdir -p system-preset
	find $(TEMPLATEDIR)/service/conf/ -type f -name "*.conf.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cname=$${filename%.template}; unit=$${cname%.conf}; compopt=$$(echo $${unit} | sed 's/-/_/g'); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -D_$${compopt}_ -DXAAS_MACRO_UNIT_NAME=$${unit} -DXAAS_MACRO_ENVIRNOMENTFILE=$${unit}".env" -DXAAS_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/system/serviceI.template system/$(ZONE_NAME).$${unit}@.service;' \;
	find conf -type f -name "*.conf" -exec bash -c 'export XDAQ_ZONE=$(ZONE_NAME); filename={}; filename=$$(basename $$filename); cname=$${filename%.template}; unit=$${cname%.conf}; export CONFIGURATION=$${unit}; set -a; source conf/$${filename%.template}; set +a; envsubst < conf/$${filename%.template} > conf/$${unit}.env' \;
	sed -i 's/@@/\/\//g' conf/*.env
	$(XAAS_ROOT)/template/scripts/instantiateXML.awk "argument=conf/xdaqd.service.conf.in" hosts.setup
	$(XAAS_ROOT)/template/scripts/instantiateTargets.awk "templatedir=$(TEMPLATEDIR)/service/system" "targetdir=system" "zone=$(ZONE_NAME)" "scope=service" hosts.setup
	find system -type f -name "*.target.cpp" -exec bash -c 'filename={}; filename=$$(basename $$filename);  cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) {} system/$${filename%.cpp}' \;
	cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME)  $(TEMPLATEDIR)/service/system-preset/preset.template system-preset/50-$(ZONE_NAME).service.preset
	sed -i 's/@@/\/\//g' system/*.target
	sed -i 's/@@/\/\//g' system/*.service

_cleanall: clean

clean:
	-rm -rf scope
	-rm -rf etc 
	-rm -f spec.template 
	-rm -rf profile/*.profile
	-rm -f hosts.setup
	-rm -f settings.xml 
	-rm -rf conf
	-rm -rf cron.d 
	-rm -rf system
	-rm -rf system-preset

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfSetupRPM.rules
