# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2020, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

BUILD_HOME:=$(shell pwd)/../../..

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
PackageName=$(shell pwd | awk -F"/" '{split($$0,a,"/");  print a[NF-1]}')
PackageType=$(shell pwd | awk -F"/" '{split($$0,a,"/");  print a[NF-2]}')
Package=$(PackageType)/$(PackageName)
ZONE_NAME=$(PackageName)

Summary=Kubernetes XaaS configuration for $(ZONE_NAME)

Description=This Helm chart provides XaaS configuration for zone $(ZONE_NAME)

Link=http://xdaq.web.cern.ch
#
# Template instantiate value
#
TEMPLATEDIR=$(XAAS_ROOT)/template/slim
CHARTNAME=$(PROJECT_NAMESPACE)$(Project)-helm-$(PackageName)

_all: all

default: all

all: clean
	mkdir -p profile
	find $(TEMPLATEDIR)/client/profile/ -type f -name "jobcontrol.profile.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*}"; cpp -P -I$(BUILD_HOME)/$(Package) -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DK8S -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} profile/$${filename}.m4; sed  -i "1i include(config.m4)dnl" profile/$${filename}.m4;' \;
	find $(TEMPLATEDIR)/service/profile/ -type f -name "*.profile.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*}"; profile="$${filename%.profile}"; cpp -P -I$(BUILD_HOME)/$(Package) -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DK8S -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} profile/$${filename}.m4; sed -i "1i include(config.m4)dnl\ndefine('\\\`'__PROFILE'\'','\\\`'$${profile}'\'')dnl" profile/$${filename}.m4' \;
	if [ -e $(BUILD_HOME)/$(Package)/service/profile ]; then \
		find $(BUILD_HOME)/$(Package)/service/profile/ -type f -name "*.profile.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*}"; profile="$${filename%.profile}"; cpp -P -I$(BUILD_HOME)/$(Package) -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DK8S -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} profile/$${filename}.m4; sed  -i "1i include(config.m4)dnl\ndefine('\\\`'__PROFILE'\'','\\\`'$${profile}'\'')dnl" profile/$${filename}.m4;' \; ; \
	fi
	if [ -e $(TEMPLATEDIR)/addon/profile ]; then \
		find $(TEMPLATEDIR)/addon/profile/ -type f -name "*.profile.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*}"; cpp -P -I$(BUILD_HOME)/$(Package) -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DK8S -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} profile/$${filename}.m4; sed  -i "1i include(config.m4)dnl" profile/$${filename}.m4;' \; ; \
	fi
	if [ -e $(BUILD_HOME)/$(Package)/addon/profile ]; then \
		find $(BUILD_HOME)/$(Package)/addon/profile/ -type f -name "*.profile.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*}"; cpp -P -I$(BUILD_HOME)/$(Package) -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DK8S -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} profile/$${filename}.m4; sed  -i "1i include(config.m4)dnl" profile/$${filename}.m4;' \; ; \
	fi
	find ./profile -type f -name "*.m4" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*}"; m4 -D__ZONE=$(ZONE_NAME) -I$(BUILD_HOME)/$(Package) {} > profile/$$filename' \;
	sed -i 's|@@|//|g' profile/*.profile
	mkdir -p sensor
	find $(TEMPLATEDIR)/settings/sensor/ -type f -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -DK8S {} sensor/$${filename}.m4; sed  -i "1i include(config.m4)dnl" sensor/$${filename}.m4;' \;
	if [ -e $(BUILD_HOME)/$(Package)/settings/sensor.template ]; then \
		find $(BUILD_HOME)/$(Package)/settings/sensor.template/ -type f -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -DK8S {} sensor/$${filename}.m4; sed  -i "1i include(config.m4)dnl" sensor/$${filename}.m4;' \; ; \
	fi
	find ./sensor -type f -name "*.m4" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*}"; m4 -D__ZONE=$(PackageName) -I$(BUILD_HOME)/$(Package) {} > sensor/$$filename' \;
	mkdir -p flash
	find $(TEMPLATEDIR)/settings/flash/ -type f -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -DK8S {} flash/$${filename}.m4; sed  -i "1i include(config.m4)dnl" flash/$${filename}.m4;' \;
	if [ -e $(BUILD_HOME)/$(Package)/settings/flash ]; then \
		find $(BUILD_HOME)/$(Package)/settings/flash/ -type f -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -DK8S {} flash/$${filename}.m4; sed  -i "1i include(config.m4)dnl" flash/$${filename}.m4;' \; ; \
	fi
	find ./flash -type f -name "*.m4" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*}"; m4 -D__ZONE=$(PackageName) -I$(BUILD_HOME)/$(Package) {} > flash/$$filename' \;
	mkdir -p store
	if [ -e $(BUILD_HOME)/$(Package)/settings/store ]; then \
		cp $(BUILD_HOME)/$(Package)/settings/store/* store/; \
	fi
	mkdir -p etc
	cp $(BUILD_HOME)/$(Package)/settings/etc/jel.xml etc/

_package: package

package:
	mkdir -p chart/$(CHARTNAME)
	helm create chart/$(CHARTNAME)
	rm chart/$(CHARTNAME)/templates/*.yaml
	rm chart/$(CHARTNAME)/templates/tests/*.yaml
	cat $(TEMPLATEDIR)/k8s/_helpers.tpl.m4 >> chart/$(CHARTNAME)/templates/_helpers.tpl
	mv chart/$(CHARTNAME)/templates/_helpers.tpl chart/$(CHARTNAME)/templates/_helpers.tpl.m4
	m4 -I$(BUILD_HOME)/$(Package) chart/$(CHARTNAME)/templates/_helpers.tpl.m4 > chart/$(CHARTNAME)/templates/_helpers.tpl
	cat $(TEMPLATEDIR)/k8s/values.yaml.m4 >> chart/$(CHARTNAME)/values.yaml
	mv chart/$(CHARTNAME)/values.yaml chart/$(CHARTNAME)/values.yaml.m4
	if [ -e values-extension.yaml.m4 ]; then \
		cat values-extension.yaml.m4 >> chart/$(CHARTNAME)/values.yaml.m4; \
	fi
	m4 -D__ZONE=$(PackageName) -I$(BUILD_HOME)/$(Package) chart/$(CHARTNAME)/values.yaml.m4 > chart/$(CHARTNAME)/values.yaml
	find $(TEMPLATEDIR)/k8s/daemonset -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/$$filename' \;
	if [ -e $(BUILD_HOME)/$(Package)/k8s/daemonset ]; then \
		find $(BUILD_HOME)/$(Package)/k8s/daemonset -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/$$filename' \; ; \
	fi
	find $(TEMPLATEDIR)/k8s/deployment -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/$$filename' \;
	if [ -e $(BUILD_HOME)/$(Package)/k8s/deployment ]; then \
		find $(BUILD_HOME)/$(Package)/k8s/deployment -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/$$filename' \; ; \
	fi
	find $(TEMPLATEDIR)/k8s/statefulset -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/$$filename' \;
	if [ -e $(BUILD_HOME)/$(Package)/k8s/statefulset ]; then \
		find $(BUILD_HOME)/$(Package)/k8s/statefulset -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/$$filename' \; ; \
	fi
	find $(TEMPLATEDIR)/k8s/namespace -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/$$filename' \;
	if [ -e $(BUILD_HOME)/$(Package)/k8s/namespace ]; then \
		find $(BUILD_HOME)/$(Package)/k8s/namespace -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/$$filename' \; ; \
	fi
	find $(TEMPLATEDIR)/k8s/service -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/service.$$filename' \;
	if [ -e $(BUILD_HOME)/$(Package)/k8s/service ]; then \
		find $(BUILD_HOME)/$(Package)/k8s/service -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/service.$$filename' \; ; \
	fi
	find $(TEMPLATEDIR)/k8s/configmap -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/configmap.$$filename' \;
	if [ -e $(BUILD_HOME)/$(Package)/k8s/configmap ]; then \
		find $(BUILD_HOME)/$(Package)/k8s/configmap -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/configmap.$$filename' \; ; \
	fi
	find $(TEMPLATEDIR)/k8s/ingress -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/ingress.$$filename' \;
	if [ -e $(BUILD_HOME)/$(Package)/k8s/ingress ]; then \
		find $(BUILD_HOME)/$(Package)/k8s/ingress -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/ingress.$$filename' \; ; \
	fi
	if [ -e $(BUILD_HOME)/$(Package)/k8s/secret ]; then \
		find $(BUILD_HOME)/$(Package)/k8s/secret -type f -name "*.yaml.template.helm" -exec bash -c 'filename={}; filename=$$(basename $$filename); filename="$${filename%.*.*}"; cp {} chart/$(CHARTNAME)/templates/secret.$$filename' \; ; \
	fi
	mkdir chart/$(CHARTNAME)/profile
	mkdir chart/$(CHARTNAME)/sensor
	mkdir chart/$(CHARTNAME)/flash
	mkdir chart/$(CHARTNAME)/store
	mkdir chart/$(CHARTNAME)/etc
	find ./profile -type f -name "*.profile" -exec bash -c 'cp {} chart/$(CHARTNAME)/profile' \;
	find ./sensor -type f \( -name "*.sensor" -o -name "*.pulser" -o -name "*.settings" \) -exec bash -c 'cp {} chart/$(CHARTNAME)/sensor' \;
	find ./flash -type f \( -name "*.flash" -o -name "*.settings" \) -exec bash -c 'cp {} chart/$(CHARTNAME)/flash' \;
	cp ./store/* chart/$(CHARTNAME)/store/
	cp ./etc/* chart/$(CHARTNAME)/etc/
	helm template --set zone.name=$(PackageName) chart/$(CHARTNAME) > helm.debug
	helm package --version "$(PACKAGE_VER_MAJOR).$(PACKAGE_VER_MINOR).$(PACKAGE_VER_PATCH)+$(PACKAGE_RELEASE).$(BUILD_VERSION)" --destination chart chart/$(CHARTNAME)

installpackage:
	mkdir -p $(INSTALL_PREFIX)/chart
	cp chart/*.tgz $(INSTALL_PREFIX)/chart

_cleanall: clean

clean:
	rm -rf profile
	rm -rf sensor
	rm -rf flash
	rm -rf etc
	rm -rf store

_cleanpackage: cleanpackage

cleanpackage:
	rm -rf chart
	rm -f helm.debug

