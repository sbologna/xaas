include(`config.m4')dnl

#
# CMSOS definitions
#
zone:
  name: __ZONE
  dockerimage: __DOCKERIMAGE
  ingress:
    host: __INGRESSHOST
  network:
    hyperdaq:
      port: __HYPERDAQPORT
      name: hyperdaq
    localnet:
      port: __LOCALNETPORT
      name: localnet
    slimnet:
      port: __SLIMNETPORT
      name: slimnet
  application:
    localbus:
      name: localbus
      profile: localbus
      replicas: __LOCALBUSREPLICAS
    jobcontrol: 
      name: jobcontrol
      profile: jobcontrol
      enabled: __JOBCONTROLENABLED
    hyperdaq:
      name: hyperdaq
      profile: hyperdaq
    xaad:
      name: xaad
      profile: xaad
    spotlight2g:
      name: spotlight2g
      profile: spotlight2g
      enabled: __SPOTLIGHT2GENABLED
    spotlightocci:
      profile: spotlightocci
      name: spotlightocci
      enabled: __SPOTLIGHTOCCIENABLED
    slimbus:
      name: slimbus
      profile: slimbus
      replicas: __SLIMBUSREPLICAS
      externalport: __SLIMBUSEXTERNALPORT
    tstore:
      name: tstore
      profile: tstore
      enabled: __TSTOREENABLED
    slash2g:
      name: slash2g
      profile: slash2g
      replicas: __SLASH2GREPLICAS
    psx:
      name: psx
      profile: psx
      managernumbers: __PSXMANAGERNUMBERS
      databasemanagers: __PSXDATABASEMANAGERS
      eventmanagers: __PSXEVENTMANAGERS
      dnss: __PSXDNSS
      replicas: __PSXREPLICAS
      enabled: __PSXENABLED
    xmasstore:
      name: xmas-store
      profile: xmas-store
      enabled: __XMASSTOREENABLED
    xmasadmin:
      name: xmas-admin
      profile: xmas-admin
      enabled: __XMASADMINENABLED
    heartbeat:
      name: heartbeat
      profile: heartbeat
      enabled: __HEARTBEATENABLED
    bridge2gsentinel:
      name: bridge2g-sentinel
      profile: bridge2g-sentinel
      enabled: __BRIDGE2GSENTINELENABLED
    bridge2gxmas:
      name: bridge2g-xmas
      profile: bridge2g-xmas
      enabled: __BRIDGE2GXMASENABLED
