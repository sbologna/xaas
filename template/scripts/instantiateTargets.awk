#! /bin/awk -f
BEGIN {
	IGNORECASE=1;
	FS=":";
	"hostname" | getline hostname;
	#print "HOSTNAME IS: [",hostname, "]";
	pattern = "^" hostname;
	targetl = "";
}

#$1 ~ pattern { 
/^[^#]/ { 
		split($1, host,".");
		target= zone "." scope "@" host[1] ".target.cpp";
		targetl =  targetl " " zone "." scope "@" host[1] ".target";
		
		#print "Zone", zone ;
		#print "Host", host[1];
		#print "Services", $2;
		#print "Template ", template;
		#print "Target File", target;

		# split services into an array and check if service to instantiate can be found
		split( $2, services, " ");
		servicel = ""; 
		for ( i in services) {
			servicel = servicel " " zone "." services[i] "@%i.service";
			#print "Service is", services[i];
		}

		# Split up the third column, presumably timers
		# etc. into components as well.
		split( $3, timers, " ");
		timerl = "";
		for ( i in timers) {
			timerl = timerl " " zone "." timers[i] "@%i.timer";
		}
		services_and_timers = servicel timerl


		#print servicel;
		targetsedcommand = "sed 's/XAAS_AWK_MACRO_SERVICES/" services_and_timers "/g' "  templatedir "/targetI.template > " targetdir "/" target ;
		
		#print "Creating host target",targetsedcommand ;
		system (targetsedcommand );
		

}

END {
	#print targetl;
	zonesedcommand = "sed 's/XAAS_AWK_MACRO_TARGETS/" targetl "/g' " templatedir"/target.template  > " targetdir "/" zone "." scope ".target.cpp";
	#print "Create zone target", zonesedcommand;
	system (zonesedcommand);
}

