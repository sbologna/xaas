There are two client-side hooks available in the root directory of the project: prepare-commit-msg and commit-msg.

"prepare-commit-msg" is called prior to firing up the commit message editor. It adds the proper prefix to the commit message, by automatically extracting issue number from the name of a feature branch.

"commit-msg" is called after the message has been edited. This hook checks the conformity of a commit message.

Hooks (both or just one of them) can be activated by creating symlinks in .git/hooks directory:
$ ln -s ../../prepare-commit-msg.py .git/hooks/prepare-commit-msg
$ ln -s ../../commit-msg.py .git/hooks/commit-msg

In situations when it is not desired to execute hooks, "--no-verify" option can be passed to a commit command.
